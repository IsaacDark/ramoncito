<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class Siniestro extends Model
{
    protected $collection = 'siniestro_collection';

    protected $connection = 'mongodb';

    protected $fillable = [
        'fecha_levantamiento',
            'hora',
            'ficha',
            'sexo',
            'edad',
            'fecha_incidente',
            'vialidad_1',
            'vialidad_2',
            'colonia',
            'causa_detallada',
            'accidente',
            'tipo_vehiculo',
            'vehiculo',
            'vialidad_uno',
            'vialidad_uno_otra',
            'carriles_uno',
            'limites_uno',
            'vialidad_uno_trans',
            'vialidad_dos',
            'vialidad_dos_otra',
            'carriles_dos',
            'limites_dos',
            'vialidad_dos_trans',
            'elementos_urbanos_vialidad_uno',
            'control_transito_vialidad_uno',
            'senaletica_horizontal_vialidad_uno',
            'senaletica_vertical_vialidad_uno',
            'elementos_urbanos_vialidad_dos',
            'control_transito_vialidad_dos',
            'senaletica_horizontal_vialidad_dos',
            'senaletica_vertical_vialidad_dos',
            'ciclovia',
            'tipo_segregada',
            'ciclista_otra',
            'tipo_segregador',
            'segregador_otra',
            'rodamiento',
            'mal_estado',
            'croquis',
            'coordenadas',
            'redaccion_hechos',
            'fotos'
    ];
}
