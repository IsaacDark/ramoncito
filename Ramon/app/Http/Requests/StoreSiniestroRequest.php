<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSiniestroRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fecha_levantamiento' =>'required',
            'hora'=> 'required',
            'ficha' => 'required',
            'sexo' => 'required',
            'edad' => 'required',
            'fecha_incidente' => 'required',
            'vialidad_1' => 'required',
            'vialidad_2' => 'required',
            'colonia' => 'required',
            'causa_detallada' => 'required',
            'accidente' => 'required',
            'tipo_vehiculo' => 'required',
            'vehiculo' => 'required',
            'vialidad_uno' => 'required_without_all:vialidad_uno_otra',
            'vialidad_uno_otra' => 'nullable|alpha',
            'carriles_uno' => 'required',
            'limites_uno' => 'required',
            'vialidad_uno_trans' => 'required',
            'vialidad_dos' => 'required_without_all:vialidad_dos_otra',
            'vialidad_dos_otra' => 'nullable|alpha',
            'carriles_dos' => 'required',
            'limites_dos' => 'required',
            'vialidad_dos_trans' => 'required',
            'elementos_urbanos_vialidad_uno' => 'required',
            'control_transito_vialidad_uno' => 'required',
            'senaletica_horizontal_vialidad_uno' => 'required',
            'senaletica_vertical_vialidad_uno' => 'required',
            'elementos_urbanos_vialidad_dos' => 'required',
            'control_transito_vialidad_dos' => 'required',
            'senaletica_horizontal_vialidad_dos' => 'required',
            'senaletica_vertical_vialidad_dos' => 'required',
            'ciclovia' => 'required',
            'tipo_segregada' => 'exclude_if:ciclovia,==,no|required_without_all:ciclista_otra',
            'ciclista_otra' => 'nullable|alpha',
            'segregador_otra' => 'nullable|alpha',
            'tipo_segregador' => 'exclude_if:ciclovia,==,no|required_without_all:segregador_otra',
            'rodamiento' => 'required_if:ciclovia,==,si',
            'mal_estado' => 'required_if:rodamiento,==,Regular|required_if:rodamiento,==,Malo',
            'croquis' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            'coordenadas' => 'required',
            'redaccion_hechos' => 'required',
            'fotos' => 'required|image'
        ];
    }

    public function attributes()
    {
        return [
            'fecha_levantamiento' => 'fecha de levantamiento',
            'fecha_incidente' => 'fecha de incidente',
            'tipo_vehiculo' =>'tipo de vehiculo',
            'carriles_uno' => 'vialidad 1 carriles',
            'limites_uno' => 'vialidad 1 limite',
            'vialidad_uno_trans' => 'vialidad 1 transporte',
            'carriles_dos' => 'vialidad 2 carriles',
            'limites_dos' => 'vialidad 2 limite',
            'vialidad_dos_trans' => 'vialidad 2 transporte',
            'control_transito' => 'control de transito',
            'senaletica_horizontal' => 'señaletica horizontal',
            'senaletica_vertical' => 'señaletica vertical',
            'ciclista_otra' => 'segregada otra',
            'tipo_segregador' => 'tipo de segregador',
            'segregador_otra' => 'segregador otra',
            'rodamiento' => 'rodamiento ciclista'
        ];
    }
}
