<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Siniestro;
use App\Traits\ImageUpload;
use App\Http\Requests\StoreSiniestroRequest;

class SiniestroController extends Controller
{
    use ImageUpload;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Siniestro::all()->last();
        $ficha = 0;
        if($id == null){
            $ficha = 1;
        }
        else{
            $ficha = $id->ficha + 1;
        } 
	    return view("moncho",['ficha' => $ficha ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSiniestroRequest $request)
    {
        
        $validated = $request->validated();

        $siniestro = new Siniestro([
            'fecha_levantamiento' => $request->fecha_levantamiento,
            'hora'=> $request->hora,
            'ficha' => $request->ficha,
            'sexo' => $request->sexo,
            'edad' => $request->edad,
            'fecha_incidente' => $request->fecha_incidente,
            'vialidad_1' => $request->vialidad_1,
            'vialidad_2' => $request->vialidad_2,
            'colonia' => $request->colonia,
            'causa_detallada' => $request->causa_detallada,
            'accidente' => $request->accidente,
            'vehiculo' => $request->vehiculo,
            'tipo_vehiculo' => $request->tipo_vehiculo,
            'vialidad_uno' => $request->vialidad_uno,
            'vialidad_uno_otra' => $request->vialidad_uno_otra,
            'carriles_uno' => $request->carriles_uno,
            'limites_uno' => $request->limites_uno,
            'vialidad_uno_trans' => $request->vialidad_uno_trans,
            'vialidad_dos' => $request->vialidad_dos,
            'vialidad_dos_otra' => $request->vialidad_dos_otra,
            'carriles_dos' => $request->carriles_dos,
            'limites_dos' => $request->limites_dos,
            'vialidad_dos_trans' => $request->vialidad_dos_trans,
            'elementos_urbanos_vialidad_uno' => $request->elementos_urbanos_vialidad_uno,
            'control_transito_vialidad_uno' => $request->control_transito_vialidad_uno,
            'senaletica_horizontal_vialidad_uno' => $request->senaletica_horizontal_vialidad_uno,
            'senaletica_vertical_vialidad_uno' => $request->senaletica_vertical_vialidad_uno,
            'elementos_urbanos_vialidad_dos' => $request->elementos_urbanos_vialidad_dos,
            'control_transito_vialidad_dos' => $request->control_transito_vialidad_dos,
            'senaletica_horizontal_vialidad_dos' => $request->senaletica_horizontal_vialidad_dos,
            'senaletica_vertical_vialidad_dos' => $request->senaletica_vertical_vialidad_dos,
            'ciclovia' => $request->ciclovia,
            'tipo_segregada' => $request->tipo_segregada,
            'ciclista_otra' => $request->ciclista_otra,
            'tipo_segregador' => $request->tipo_segregador,
            'segregador_otra' => $request->segregador_otra,
            'rodamiento' => $request->rodamiento,
            'mal_estado' => $request->mal_estado,
            'croquis' => $request->croquis,
            'coordenadas' => $request->coordenadas,
            'redaccion_hechos' => $request->redaccion_hechos,
            'fotos' => $request->fotos
        ]);

        if($siniestro->croquis && $siniestro->fotos){
            try {
                $filePath = $this->SiniestroImageUpload($siniestro->croquis);
                $filaPath2 = $this->SiniestroImageUpload($siniestro->fotos);  //Passing $data->image as parameter to our created method
                $siniestro->croquis = $filePath;
                $siniestro->fotos = $filaPath2;
                $siniestro->save();
                return view('hola');

            } catch (Exception $e) {
           //Write your error message here
            }
        }
        
        $siniestro->save();
        
        return view('ramon');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
