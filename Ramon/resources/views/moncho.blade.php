<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css') }}">
        <!-- JavaScript Bundle with Popper -->
        <!-- Font Awesome -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
        <script
  src="https://code.jquery.com/jquery-3.6.0.js"
  integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
  crossorigin="anonymous"></script>
        <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('locale/bootstrap-datepicker.es.min.js') }}"></script>
        <title>Siniestro</title>
    </head>
    <body>
        <div class="container-fluid">
            <!-- Content here -->
            <nav class="navbar navbar-expand-lg navbar-light bg-light mb-3">
                <div class="container-md">
                    <a class="navbar-brand fw-bold fs-2" href="#">Hechos viales ciclistas</a>
                    <a class="navbar-brand fw-bold fs-2" href="{{ route('inicio') }}">volver</a>
                </div>
            </nav>
            <!--
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> datos requeridos<br><br>
                    <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
            @endif
            -->
            <form action="{{ route('siniestro.store') }}" method="POST" enctype="multipart/form-data" autocomplete="off">
                {{ csrf_field() }}
                <!-- primera fila -->
                <div class="row">
                    <div class="mb-3 col-2">
                        <span class="input-group-text" id="basic-addon1">Fecha de levantamiento</span>
                    </div>
                    <div class="mb-3 col-3">
                        <input type="text" class="form-control @error('fecha_levantamiento') is-invalid @enderror" name="fecha_levantamiento" value="{{ old('fecha_levantamiento') }}" id="date" readonly>
                        @error('fecha_levantamiento')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="mb-3 col-1">
                        <span class="input-group-text"  id="basic-addon1">Hora</span>
                    </div>
                    <div class="mb-3 col-2">
                        <input type="time" class="form-control @error('hora') is-invalid @enderror" name="hora" value="{{ old('hora') }}" id="1">
                        @error('hora')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-3 col-1">
                        <span class="input-group-text" id="basic-addon1">N° Ficha</span>
                    </div>
                    <div class="mb-3 col-3">
                        <input type="text" class="form-control @error('ficha') is-invalid @enderror"  name="ficha" value={{$ficha}} placeholder="" aria-label="Username" aria-describedby="basic-addon1" id="2" autocomplete="off" readonly>
                        @error('ficha')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <!-- segunda fila -->
                <div class="row">
                    <div class="mb-3 col-2">
                        <span class="input-group-text" id="basic-addon1">Fecha del incidente</span>
                    </div>
                    <div class="mb-3 col-4">
                        <input type="text" class="form-control @error('fecha_incidente') is-invalid @enderror" name="fecha_incidente" value="{{ old('fecha_incidente') }}"  aria-label="Username" aria-describedby="basic-addon1" id="6" readonly>
                        @error('fecha_incidente')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-3 col-1">
                        <span class="input-group-text" id="basic-addon1">Edad</span>
                    </div>
                    <div class="mb-3 col-2">
                        <input type="text" class="form-control @error('edad') is-invalid @enderror" name="edad" value="{{ old('edad') }}"  placeholder="Edad del involucrado" aria-label="Username" aria-describedby="basic-addon1" id="5">
                        @error('edad')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="mb-3 col-1">
                        <span class="input-group-text" id="basic-addon1">Sexo</span>
                    </div>
                    <div class="mb-3 col-2">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="radio" name="sexo" value="H" @if(old('sexo') == 'H') checked @endif id="3">
                            <label class="form-check-label" for="3">H</label>
                          </div>
                          <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="radio" name="sexo"  value="M" @if(old('sexo') == 'M') checked @endif id="4">
                            <label class="form-check-label" for="4">M</label>
                          </div>
                            @error('sexo')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                    </div>
                </div>
                <!-- tercera fila -->
                <div class="row">
                    <div class="input-group">
                        <span class="input-group-text">Vialidad 1</span>
                        <input type="text" class="form-control @error('vialidad_1') is-invalid @enderror" name="vialidad_1" value="{{ old('vialidad_1') }}" placeholder="Vialidad Principal" aria-label="Username" id="7">
                        <span class="input-group-text">Vialidad 2</span>
                        <input type="text" class="form-control @error('vialidad_2') is-invalid @enderror" name="vialidad_2" value="{{ old('vialidad_2') }}" placeholder="Vialidad Secundaria" aria-label="Server" id="8">
                      </div>
                </div>
                <div class="row mb-3">
                    <div class="col-1">
                    </div>
                    <div class="col-6">
                        @error('vialidad_1')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-5">
                        @error('vialidad_2')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <!-- cuarta fila -->
                <div class="row">
                    <div class="input-group">
                        <span class="input-group-text">Colonia</span>
                        <input type="text" class="form-control @error('colonia') is-invalid @enderror" name="colonia" value="{{ old('colonia') }}" placeholder="Nombre de la colonia" aria-label="Username" id="9">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-1">
                    </div>
                    <div class="col-6">
                        @error('colonia')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <!-- quinta fila -->
                <div class="row">
                    <div class="input-group">
                        <span class="input-group-text">Tipo del incidente</span>
                        <input type="text" class="form-control @error('causa_detallada') is-invalid @enderror" name="causa_detallada" value="{{ old('causa_detallada') }}" placeholder="Redacte el incidente de forma detallada" aria-label="Username" id="10">
                      </div>
                </div>
                <div class="row mb-3">
                    <div class="col-2">
                    </div>
                    <div class="col-6">
                        @error('causa_detallada')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <!-- sexta fila -->
                <div class="row">
                    <div class="mb-3 col-2">
                        <span class="input-group-text" id="basic-addon1">¿Dónde fue el incidente?</span>
                    </div>
                    <div class="mb-3 col-3">
                        
                        <div class="form-check form-check-inline mt-2">
                            <label class="form-check-label" for="11">A mitad de una calle</label>
                            <input class="form-check-input" type="radio" name="accidente" value="A mitad de una calle" @if(old('accidente') == 'A mitad de una calle') checked @endif id="11">
                        </div>
                    </div>
                    <div class="mb-3 col-3">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="radio" name="accidente"  value="En intersección" @if(old('accidente') == 'En intersección') checked @endif id="12">
                            <label class="form-check-label" for="12">En intersección</label>
                        </div>
                    </div>
                    <div class="mb-3 col-3">
                        @error('accidente')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <!-- septima fila -->
                <div class="row">
                    <div class="mb-3 col-2">
                        <span class="input-group-text" id="basic-addon1">Tipo de vehículo Involucrado</span>
                    </div>
                    <div class="mb-3 col-2">
                        
                        <div class="form-check form-check-inline mt-2">
                            <label class="form-check-label" for="13">Transporte público</label>
                            <input class="form-check-input" type="radio" name="tipo_vehiculo"  value="Transporte Publico" @if(old('tipo_vehiculo') == 'Transporte Publico') checked @endif id="13" onclick="adios()">
                            <input type="text" class="form-control @error('vehiculo') is-invalid @enderror" name="vehiculo" value="{{ old('vehiculo') }}" placeholder="Tipo de unidad" aria-label="Username" id="997">
                            
                        </div>
                    </div>
                    <div class="mb-3 col-2">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="radio" name="tipo_vehiculo"  value="Transporte De Carga" @if(old('tipo_vehiculo') == 'Transporte De Carga') checked @endif id="14" onclick="adios()">
                            <label class="form-check-label" for="14">Transporte de carga</label>
                            <input type="text" class="form-control @error('vehiculo') is-invalid @enderror" name="vehiculo" value="{{ old('vehiculo') }}" placeholder="Tipo de unidad" aria-label="Username" id="998">
                        </div>
                    </div>
                    <div class="mb-3 col-2">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="radio" name="tipo_vehiculo"  value="Transporte Particular" @if(old('tipo_vehiculo') == 'Transporte Particular') checked @endif id="15" onclick="adios()">
                            <label class="form-check-label" for="15">Vehículo particular</label>
                            <input type="text" class="form-control @error('vehiculo') is-invalid @enderror" name="vehiculo" value="{{ old('vehiculo') }}" placeholder="Tipo de unidad" aria-label="Username" id="999">
                        </div>
                    </div>
                    <div class="mb-3 col-2">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="radio" name="tipo_vehiculo"  value="otro" @if(old('tipo_vehiculo') == 'otro') checked @endif id="80" onclick="adios()">
                            <label class="form-check-label" for="80">otro</label>
                            <input type="text" class="form-control @error('vehiculo') is-invalid @enderror" name="vehiculo" value="{{ old('vehiculo') }}" placeholder="Tipo de unidad" aria-label="Username" id="1999">
                        </div>
                    </div>
                    <div class="mb-3 col-1">
                        @error('tipo_vehiculo')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                        @error('vehiculo')
                          <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <!-- octava fila -->
                <div class="row">
                    <div class="col-12 mb-0">
                    <p class="bg-secondary p-2 text-center fw-bold fs-3" id="basic-addon1">Tipo de vialidades donde fue el incidente</p>
                    </div>
                </div>
                <!-- novena fila -->
                <div class="row">
                    <div class="col-12 mb-3">
                        <p class="bg-secondary bg-gradient p-1 text-center fw-bold fs-4" id="basic-addon1">Vialidad 1</p>
                    </div>
                </div>
                <!-- decima fila  vialidad 1-->
                <div class="row mb-3">
                    
                    <div class="col-2">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="vialidad_uno[]" value="Metropolitana" @if(is_array(old('vialidad_uno')) && in_array("Metropolitana",old('vialidad_uno'))) checked @endif id="16">
                            <label class="form-check-label" for="16">Metropolitana</label>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="vialidad_uno[]"  value="Principal" @if(is_array(old('vialidad_uno')) && in_array("Principal",old('vialidad_uno'))) checked @endif id="17">
                            <label class="form-check-label" for="17">Principal</label>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="vialidad_uno[]"  value="Colectora" @if(is_array(old('vialidad_uno')) && in_array("Colectora",old('vialidad_uno'))) checked @endif id="18">
                            <label class="form-check-label" for="18">Colectora</label>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="vialidad_uno[]" value="Local" @if(is_array(old('vialidad_uno')) && in_array("Local",old('vialidad_uno'))) checked @endif id="19">
                            <label class="form-check-label" for="19">Local</label>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="vialidad_uno[]" value="Prioridad ciclista" @if(is_array(old('vialidad_uno')) && in_array("Prioridad ciclista",old('vialidad_uno'))) checked @endif id="20">
                            <label class="form-check-label" for="20">Prioridad ciclista</label>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="input-group mb-3">
                            <span class="input-group-text">Otra</span>
                            <input type="text" class="form-control @error('vialidad_uno_otra') is-invalid @enderror" name="vialidad_uno_otra" value="{{ old('vialidad_uno_otra') }}" placeholder="Especifique" aria-label="Username" id="21">
                        </div>
                        @error('vialidad_uno_otra')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    @error('vialidad_uno')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <!-- onceaba fila -->
                <div class="row">
                    <div class="col-2 mb-3">
                        <span class="input-group-text" id="basic-addon1">N° carriles</span>
                    </div>
                    <div class="col-3 mb-3">
                        <input type="text" class="form-control @error('carriles_uno') is-invalid @enderror" name="carriles_uno" value="{{ old('carriles_uno') }}" placeholder="Número de Carriles" aria-label="Username" id="22">
                        @error('carriles_uno')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-3 mb-3">
                        <span class="input-group-text" id="basic-addon1">Límite velocidad</span>
                    </div>
                    <div class="col-4 mb-3">
                        <input type="text" class="form-control @error('limites_uno') is-invalid @enderror"name="limites_uno" value="{{ old('limites_uno') }}" placeholder="Limite de km/h" aria-label="Username" id="23">
                        @error('limites_uno')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                    </div>
                    <div class="row">
                        <div class="col-2 mb-3">
                            <span class="input-group-text" id="basic-addon1">En esta vialidad Transita</span>
                        </div>
                        <div class="col-8 mb-3">
                            <div class="form-check form-check-inline mt-2">
                                <input class="form-check-input" type="checkbox" name="vialidad_uno_trans[]"  value="Transporte Publico" @if(is_array(old('vialidad_uno_trans')) && in_array("Transporte Publico",old('vialidad_uno_trans'))) checked @endif id="24">
                                <label class="form-check-label" for="24">Transp. público</label>
                            </div>
                            <div class="form-check form-check-inline mt-2">
                                <input class="form-check-input" type="checkbox" name="vialidad_uno_trans[]" value="Transporte de Carga" @if(is_array(old('vialidad_uno_trans')) && in_array("Transporte de Carga",old('vialidad_uno_trans'))) checked @endif id="25">
                                <label class="form-check-label" for="25">Transp. carga</label>
                            </div>
                            <div class="form-check form-check-inline mt-2">
                                <input class="form-check-input" type="checkbox" name="vialidad_uno_trans[]"  value="Transporte Masivo" @if(is_array(old('vialidad_uno_trans')) && in_array("Transporte Masivo",old('vialidad_uno_trans'))) checked @endif id="24">
                                <label class="form-check-label" for="24">Transp. masivo</label>
                            </div>
                            <div class="form-check form-check-inline mt-2">
                                <input class="form-check-input" type="checkbox" name="vialidad_uno_trans[]"  value="No transita" @if(is_array(old('vialidad_uno_trans')) && in_array("No transita",old('vialidad_uno_trans'))) checked @endif id="24">
                                <label class="form-check-label" for="24">No transita</label>
                            </div>
                            @error('vialidad_uno_trans')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-4 mb-3">
                        <p class="bg-secondary bg-gradient p-1 text-center fw-bold fs-4" id="basic-addon1">Elementos urbanos</p>
                    </div>
                    
                    <div class="col-3 mb-3">
                        <p class="bg-light bg-gradient p-1 text-center fw-bold fs-4" id="basic-addon1">Disp de control de transito</p>
                    </div>
                    <div class="col-5 mb-3">
                        <p class="bg-secondary bg-gradient p-1 text-center fw-bold fs-4" id="basic-addon1">Señalamiento</p>
                    </div>
                </div>
                <!-- 16 fila EDS vialidad 1-->
                <div class="row">
                    <div class="col-2 mx-2 mb-3">
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_uno[]"  value="Arbolado-1" @if(is_array(old('elementos_urbanos')) && in_array("Arbolado-1",old('elementos_urbanos'))) checked @endif id="400">
                            <label class="form-check-label" for="400">
                            Arbolado
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_uno[]"  value="Parabús-1" @if(is_array(old('elementos_urbanos')) && in_array("Parabús-1",old('elementos_urbanos'))) checked @endif id="401">
                            <label class="form-check-label" for="401">
                            Parabús
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_uno[]" value="Luminarias-1" @if(is_array(old('elementos_urbanos')) && in_array("Luminarias-1",old('elementos_urbanos'))) checked @endif id="402">
                            <label class="form-check-label" for="402">
                            Luminarias
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_uno[]" id="500" value="Cadenas-1" @if(is_array(old('elementos_urbanos')) && in_array("Cadenas-1",old('elementos_urbanos'))) checked @endif>
                            <label class="form-check-label" for="403">
                            Cadenas
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_uno[]" id="501"  value="Poste-1" @if(is_array(old('elementos_urbanos')) && in_array("Poste-1",old('elementos_urbanos'))) checked @endif>
                            <label class="form-check-label" for="404">
                            Poste
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_uno[]" id="502"  value="Puesto de periódicos-1" @if(is_array(old('elementos_urbanos')) && in_array("Puesto de periódicos-1",old('elementos_urbanos'))) checked @endif>
                            <label class="form-check-label" for="405">
                            Puesto de periódicos
                            </label>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_uno[]" id="503"  value="Telefono público-1" @if(is_array(old('elementos_urbanos')) && in_array("Telefono público-1",old('elementos_urbanos'))) checked @endif>
                            <label class="form-check-label" for="406">
                            Teléfono público
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_uno[]" id="504"  value="Bocas de tormenta-1" @if(is_array(old('elementos_urbanos')) && in_array("Bocas de tormenta-1",old('elementos_urbanos'))) checked @endif>
                            <label class="form-check-label" for="407">
                            Bocas de tormenta
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_uno[]" id="505"  value="Totem publicitario-1" @if(is_array(old('elementos_urbanos')) && in_array("Totem publicitario-1",old('elementos_urbanos'))) checked @endif>
                            <label class="form-check-label" for="408">
                            Totem publicitario
                            </label>
                        </div>
                    </div>
                    <div class="col-1"></div>
                    <div class="col-2">
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="control_transito_vialidad_uno[]" id="506" value="Semaforos-1" @if(is_array(old('control_transito')) && in_array("Semaforos-1",old('control_transito'))) checked @endif> 
                            <label class="form-check-label" for="409">
                            Semáforos
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="control_transito_vialidad_uno[]" id="507" value="Topes-1" @if(is_array(old('control_transito')) && in_array("Topes-1",old('control_transito'))) checked @endif>
                            <label class="form-check-label" for="410">
                            Topes
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="control_transito_vialidad_uno[]" id="508"  value="Vialidades o vialeton-1" @if(is_array(old('control_transito')) && in_array("Vialidades o vialeton-1",old('control_transito'))) checked @endif>
                            <label class="form-check-label" for="411">
                            Vialeta o vialeton
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="control_transito_vialidad_uno[]" id="509"  value="Boyas-1" @if(is_array(old('control_transito')) && in_array("Boyas-1",old('control_transito'))) checked @endif>
                            <label class="form-check-label" for="412">
                            Boyas
                            </label>
                        </div>
                    </div>
                    <div class="col-2 mx-auto">
                        <h5>Horizontal</h5>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_horizontal_vialidad_uno[]" id="510"  value="Buffer-1" @if(is_array(old('senaletica_horizontal')) && in_array("Buffer-1",old('senaletica_horizontal'))) checked @endif> 
                            <label class="form-check-label" for="413">
                            Buffer
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_horizontal_vialidad_uno[]" id="511"  value="Cajabici-1" @if(is_array(old('senaletica_horizontal')) && in_array("Cajabici-1",old('senaletica_horizontal'))) checked @endif>
                            <label class="form-check-label" for="414">
                            Cajabici
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_horizontal_vialidad_uno[]" id="512" value="Flechas de dirección-1" @if(is_array(old('senaletica_horizontal')) && in_array("Flechas de dirección-1",old('senaletica_horizontal'))) checked @endif>
                            <label class="form-check-label" for="415">
                            Flechas de dirección
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_horizontal_vialidad_uno[]" id="513"  value="Paso peatonal-1" @if(is_array(old('senaletica_horizontal')) && in_array("Paso peatonal-1",old('senaletica_horizontal'))) checked @endif>
                            <label class="form-check-label" for="416">
                            Paso peatonal
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_horizontal_vialidad_uno[]" id="514"  value="Línea de alto-1" @if(is_array(old('senaletica_horizontal')) && in_array("Línea de alto-1",old('senaletica_horizontal'))) checked @endif>
                            <label class="form-check-label" for="417">
                            Línea de alto
                            </label>
                        </div>
                    </div>
                    <div class="col-2">
                        <h5>Vertical</h5>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_vertical_vialidad_uno[]" id="515" value="Alto-1" @if(is_array(old('senaletica_vertical')) && in_array("Alto-1",old('senaletica_vertical'))) checked @endif>
                            <label class="form-check-label" for="418">
                            Alto
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_vertical_vialidad_uno[]" id="516"value="Límite de velocidad-1" @if(is_array(old('senaletica_vertical')) && in_array("Límite de velocidad-1",old('senaletica_vertical'))) checked @endif>
                            <label class="form-check-label" for="418">
                            Límite de velocidad
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_vertical_vialidad_uno[]" id="517"  value="Circulación-1" @if(is_array(old('senaletica_vertical')) && in_array("Circulación-1",old('senaletica_vertical'))) checked @endif>
                            <label class="form-check-label" for="419">
                            Circulación
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_vertical_vialidad_uno[]" id="518"  value="Preventiva-1" @if(is_array(old('senaletica_vertical')) && in_array("Preventiva-1",old('senaletica_vertical'))) checked @endif>
                            <label class="form-check-label" for="420">
                            Preventiva
                            </label>
                        </div>
                </div>
                <!-- doceaba fila -->
                <div class="row">
                    <div class="col-12 mb-3">
                        <p class="bg-secondary bg-gradient p-1 text-center fw-bold fs-4" id="basic-addon1">Vialidad 2</p>
                    </div>
                </div>
                <!-- 13era fila  vialidad 2-->
                <div class="row mb-3">
                    <div class="col-2">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="vialidad_dos[]" value="Metropolitana" @if(is_array(old('vialidad_dos')) && in_array("Metropolitana",old('vialidad_dos'))) checked @endif id="26">
                            <label class="form-check-label" for="26">Metropolitana</label>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="vialidad_dos[]" value="Principal" @if(is_array(old('vialidad_dos')) && in_array("Principal",old('vialidad_dos'))) checked @endif id="27">
                            <label class="form-check-label" for="27">Principal</label>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="vialidad_dos[]"  value="Colectora" @if(is_array(old('vialidad_dos')) && in_array("Colectora",old('vialidad_dos'))) checked @endif id="28">
                            <label class="form-check-label" for="28">Colectora</label>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="vialidad_dos[]"  value="Local" @if(is_array(old('vialidad_dos')) && in_array("Local",old('vialidad_dos'))) checked @endif id="74">
                            <label class="form-check-label" for="74">Local</label>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="vialidad_dos[]" value="Prioridad ciclista" @if(is_array(old('vialidad_dos')) && in_array("Prioridad ciclista",old('vialidad_dos'))) checked @endif id="29">
                            <label class="form-check-label" for="29">Prioridad ciclista</label>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="input-group mb-3">
                            <span class="input-group-text">Otra</span>
                            <input type="text" class="form-control @error('vialidad_dos_otra') is-invalid @enderror" name="vialidad_dos_otra" value="{{ old('vialidad_dos_otra') }}" placeholder="Especifique" aria-label="Username" id="30">
                        </div>
                        @error('vialidad_dos_otra')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    @error('vialidad_dos')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <!-- 14 fila -->
                <div class="row">
                    <div class="col-2 mb-3">
                        <span class="input-group-text" id="basic-addon1">N° carriles</span>
                    </div>
                    <div class="col-3 mb-3">
                        <input type="text" class="form-control @error('carriles_dos') is-invalid @enderror" name="carriles_dos" value="{{ old('carriles_dos') }}" placeholder="Numero de carriles" aria-label="Username" id="31">
                        @error('carriles_dos')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-3 mb-3">
                        <span class="input-group-text" id="basic-addon1">Límite velocidad</span>
                    </div>
                    <div class="col-4 mb-3">
                        <input type="text" class="form-control @error('limites_dos') is-invalid @enderror" name="limites_dos" value="{{ old('limites_dos') }}" placeholder="Limite de km/h" aria-label="Username" id="32">
                        @error('limites_dos')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-2 mb-3">
                        <span class="input-group-text" id="basic-addon1">En esta vialidad Transita</span>
                    </div>
                    <div class="col-8 mb-3">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="vialidad_dos_trans[]" value="Transporte Publico" @if(is_array(old('vialidad_dos_trans')) && in_array("Transporte Publico",old('vialidad_dos_trans'))) checked @endif id="33">
                            <label class="form-check-label" for="33">Transp. público</label>
                        </div>
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="vialidad_dos_trans[]" value="Transporte de Carga" @if(is_array(old('vialidad_dos_trans')) && in_array("Transporte de Carga",old('vialidad_dos_trans'))) checked @endif id="34">
                            <label class="form-check-label" for="34">Transp. carga</label>
                        </div>
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="vialidad_dos_trans[]" value="Transporte Masivo" @if(is_array(old('vialidad_dos_trans')) && in_array("Transporte Masivo",old('vialidad_dos_trans'))) checked @endif id="102">
                            <label class="form-check-label" for="34">Transp. masivo</label>
                        </div>
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="vialidad_dos_trans[]" value="No transita" @if(is_array(old('vialidad_dos_trans')) && in_array("No transita",old('vialidad_dos_trans'))) checked @endif id="103">
                            <label class="form-check-label" for="34">No transita</label>
                        </div>
                        @error('vialidad_dos_trans')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <!-- 15 fila -->
                <div class="row">
                    <div class="col-4 mb-3">
                        <p class="bg-secondary bg-gradient p-1 text-center fw-bold fs-4" id="basic-addon1">Elementos urbanos</p>
                    </div>
                    
                    <div class="col-3 mb-3">
                        <p class="bg-light bg-gradient p-1 text-center fw-bold fs-4" id="basic-addon1">Disp de control de transito</p>
                    </div>
                    <div class="col-5 mb-3">
                        <p class="bg-secondary bg-gradient p-1 text-center fw-bold fs-4" id="basic-addon1">Señalamiento</p>
                    </div>
                </div>
                <!-- 16 fila EDS vialidad 2 -->
                <div class="row">
                    <div class="col-2 mx-2 mb-3">
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_dos[]"  value="Arbolado" @if(is_array(old('elementos_urbanos')) && in_array("Arbolado",old('elementos_urbanos'))) checked @endif id="35">
                            <label class="form-check-label" for="35">
                            Arbolado
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_dos[]"  value="Parabús" @if(is_array(old('elementos_urbanos')) && in_array("Parabús",old('elementos_urbanos'))) checked @endif id="36">
                            <label class="form-check-label" for="36">
                            Parabús
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_dos[]" value="Luminarias" @if(is_array(old('elementos_urbanos')) && in_array("Luminarias",old('elementos_urbanos'))) checked @endif id="37">
                            <label class="form-check-label" for="37">
                            Luminarias
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_dos[]" id="38" value="Cadenas" @if(is_array(old('elementos_urbanos')) && in_array("Cadenas",old('elementos_urbanos'))) checked @endif>
                            <label class="form-check-label" for="38">
                            Cadenas
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_dos[]" id="39"  value="Poste" @if(is_array(old('elementos_urbanos')) && in_array("Poste",old('elementos_urbanos'))) checked @endif>
                            <label class="form-check-label" for="39">
                            Poste
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_dos[]" id="40"  value="Puesto de periódicos" @if(is_array(old('elementos_urbanos')) && in_array("Puesto de periódicos",old('elementos_urbanos'))) checked @endif>
                            <label class="form-check-label" for="40">
                            Puesto de periódicos
                            </label>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_dos[]" id="41"  value="Telefono público" @if(is_array(old('elementos_urbanos')) && in_array("Telefono público",old('elementos_urbanos'))) checked @endif>
                            <label class="form-check-label" for="41">
                            Teléfono público
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_dos[]" id="42"  value="Bocas de tormenta" @if(is_array(old('elementos_urbanos')) && in_array("Bocas de tormenta",old('elementos_urbanos'))) checked @endif>
                            <label class="form-check-label" for="42">
                            Bocas de tormenta
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="elementos_urbanos_vialidad_dos[]" id="43"  value="Totem publicitario" @if(is_array(old('elementos_urbanos')) && in_array("Totem publicitario",old('elementos_urbanos'))) checked @endif>
                            <label class="form-check-label" for="43">
                            Totem publicitario
                            </label>
                        </div>
                    </div>
                    <div class="col-1"></div>
                    <div class="col-2">
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="control_transito_vialidad_dos[]" id="44" value="Semaforos" @if(is_array(old('control_transito')) && in_array("Semaforos",old('control_transito'))) checked @endif> 
                            <label class="form-check-label" for="44">
                            Semáforos
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="control_transito_vialidad_dos[]" id="45" value="Topes" @if(is_array(old('control_transito')) && in_array("Topes",old('control_transito'))) checked @endif>
                            <label class="form-check-label" for="45">
                            Topes
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="control_transito_vialidad_dos[]" id="46"  value="Vialeta o vialeton" @if(is_array(old('control_transito')) && in_array("Vialeta o vialeton",old('control_transito'))) checked @endif>
                            <label class="form-check-label" for="46">
                            Vialeta o vialeton
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="control_transito_vialidad_dos[]" id="47"  value="Boyas" @if(is_array(old('control_transito')) && in_array("Boyas",old('control_transito'))) checked @endif>
                            <label class="form-check-label" for="47">
                            Boyas
                            </label>
                        </div>
                    </div>
                    <div class="col-2 mx-auto">
                        <h5>Horizontal</h5>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_horizontal_vialidad_dos[]" id="48"  value="Buffer" @if(is_array(old('senaletica_horizontal')) && in_array("Buffer",old('senaletica_horizontal'))) checked @endif> 
                            <label class="form-check-label" for="48">
                            Buffer
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_horizontal_vialidad_dos[]" id="49"  value="Cajabici" @if(is_array(old('senaletica_horizontal')) && in_array("Cajabici",old('senaletica_horizontal'))) checked @endif>
                            <label class="form-check-label" for="49">
                            Cajabici
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_horizontal_vialidad_dos[]" id="50" value="Flechas de dirección" @if(is_array(old('senaletica_horizontal')) && in_array("Flechas de dirección",old('senaletica_horizontal'))) checked @endif>
                            <label class="form-check-label" for="50">
                            Flechas de dirección
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_horizontal_vialidad_dos[]" id="51"  value="Paso peatonal" @if(is_array(old('senaletica_horizontal')) && in_array("Paso peatonal",old('senaletica_horizontal'))) checked @endif>
                            <label class="form-check-label" for="51">
                            Paso peatonal
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_horizontal_vialidad_dos[]" id="52"  value="Línea de alto" @if(is_array(old('senaletica_horizontal')) && in_array("Línea de alto",old('senaletica_horizontal'))) checked @endif>
                            <label class="form-check-label" for="52">
                            Línea de alto
                            </label>
                        </div>
                    </div>
                    <div class="col-2">
                        <h5>Vertical</h5>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_vertical_vialidad_dos[]" id="53" value="Alto" @if(is_array(old('senaletica_vertical')) && in_array("Alto",old('senaletica_vertical'))) checked @endif>
                            <label class="form-check-label" for="53">
                            Alto
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_vertical_vialidad_dos[]" id="54"value="Límite de velocidad" @if(is_array(old('senaletica_vertical')) && in_array("Límite de velocidad",old('senaletica_vertical'))) checked @endif>
                            <label class="form-check-label" for="54">
                            Límite de velocidad
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_vertical_vialidad_dos[]" id="55"  value="Circulación" @if(is_array(old('senaletica_vertical')) && in_array("Circulación",old('senaletica_vertical'))) checked @endif>
                            <label class="form-check-label" for="55">
                            Circulación
                            </label>
                        </div>
                        <div class="form-check mt-2">
                            <input class="form-check-input" type="checkbox" name="senaletica_vertical_vialidad_dos[]" id="56"  value="Preventiva" @if(is_array(old('senaletica_vertical')) && in_array("Preventiva",old('senaletica_vertical'))) checked @endif>
                            <label class="form-check-label" for="56">
                            Preventiva
                            </label>
                        </div>
                    </div>  
                </div>
                <!-- 17 fila -->
                <div class="row mb-3">
                    <div class="col-3">
                        @error('elementos_urbanos')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-3">
                        @error('control_transito')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-3">
                        @error('senaletica_horizontal')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-3">
                        @error('senaletica_vertical')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 mb-3">
                        <p class="bg-secondary bg-gradient p-1 text-center fw-bold fs-4" id="basic-addon1">Infrestructura ciclista</p>
                    </div>
                </div>
                <!-- 18 fila -->
                <div class="row">
                    <div class="col-2 mb-3">
                        <span class="input-group-text" id="basic-addon1">¿Hay ciclovia?</span>
                    </div>
                    <div class="mb-3 col-2">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="radio" onclick="hola()" name="ciclovia" id="57" value="si" @if(old('ciclovia') == 'si') checked @endif>
                            <label class="form-check-label" for="57">Sí</label>
                          </div>
                          <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="radio" onclick="hola()" name="ciclovia" id="58"  value="no" @if(old('ciclovia') == 'no') checked @endif>
                            <label class="form-check-label" for="58">No</label>
                          </div>
                          
                        @error('ciclovia')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                        
                    </div>
                    <div class="col-2 mb-3">
                        <span class="input-group-text" id="basic-addon1">Tipo</span>
                    </div>
                    <div class="mb-3 col-4">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="tipo_segregada[]" id="59"  value="Nivel de banqueta" @if(is_array(old('tipo_segregada')) && in_array("Nivel de banqueta",old('tipo_segregada'))) checked @endif>
                            <label class="form-check-label" for="59">Nivel de banqueta</label>
                          </div>
                          <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="tipo_segregada[]" id="60" value="Confinada" @if(is_array(old('tipo_segregada')) && in_array("Confinada",old('tipo_segregada'))) checked @endif> 
                            <label class="form-check-label" for="60">Confinada</label>
                          </div>
                          <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="tipo_segregada[]" id="61"  value="Segregada" @if(is_array(old('tipo_segregada')) && in_array("Segregada",old('tipo_segregada'))) checked @endif>
                            <label class="form-check-label" for="61">Segregada</label>
                          </div>
                        @error('tipo_segregada')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-2 mb-3">
                        <div class="input-group mb-3">
                            <span class="input-group-text">Otra</span>
                            <input type="text" class="form-control" name="ciclista_otra" value="{{ old('ciclista_otra') }}" placeholder="Especifique" aria-label="Username" id="200">
                            @error('ciclista_otra')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <!-- 19 fila -->
                <div class="row">
                    <div class="col-3 mb-3">
                        <span class="input-group-text" id="basic-addon1">Tipo de segregador</span>
                    </div>
                    <div class="col-6 mb-3">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="tipo_segregador[]" id="62"  value="Flauta" @if(is_array(old('tipo_segregador')) && in_array("Flauta",old('tipo_segregador'))) checked @endif>
                            <label class="form-check-label" for="62">Flauta</label>
                        </div>
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="tipo_segregador[]" id="63" value="Quesadilla" @if(is_array(old('tipo_segregador')) && in_array("Quesadilla",old('tipo_segregador'))) checked @endif>
                            <label class="form-check-label" for="63">Quesadilla</label>
                        </div>
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="tipo_segregador[]" id="64" value="Bolardo A" @if(is_array(old('tipo_segregador')) && in_array("Bolardo A",old('tipo_segregador'))) checked @endif>
                            <label class="form-check-label" for="64">Bolardo abatible</label>
                        </div>
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="tipo_segregador[]" id="65"  value="Vialetas" @if(is_array(old('tipo_segregador')) && in_array("Vialetas",old('tipo_segregador'))) checked @endif>
                            <label class="form-check-label" for="65">Vialetas</label>
                        </div>
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="checkbox" name="tipo_segregador[]" id="66" value="Buffer" @if(is_array(old('tipo_segregador')) && in_array("Buffer",old('tipo_segregador'))) checked @endif>
                            <label class="form-check-label" for="66">Buffer</label>
                        </div>
                    </div>

                    <div class="col-3 mb-3">
                        <div class="input-group mb-3">
                            <span class="input-group-text">Otra</span>
                            <input type="text" class="form-control" name="segregador_otra" value="{{ old('segregador_otra') }}" placeholder="Especifique" aria-label="Username" id="67">
                            @error('segregador_otra')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-3"></div>
                    <div class="col-6">
                        @error('tipo_segregador')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <!-- 20 fila -->
                <div class="row">
                    <div class="col-2 mb-3">
                        <span class="input-group-text" id="basic-addon1">Superficie de rodamiento</span>
                    </div>
                    <div class="col-2 mb-3">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="radio" name="rodamiento" onclick="rodamientos()" id="68"  value="Bueno" @if(old('rodamiento') == 'Bueno') checked @endif>
                            <label class="form-check-label" for="68">Bueno</label>
                        </div>
                    </div>
                    <div class="col-2 mb-3">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="radio" name="rodamiento" onclick="rodamientos()" id="69" value="Regular" @if(old('rodamiento') == 'Regular') checked @endif>
                            <label class="form-check-label" for="69">Regular</label>
                        </div>
                    </div>
                    <div class="col-2 mb-3">
                        <div class="form-check form-check-inline mt-2">
                            <input class="form-check-input" type="radio" name="rodamiento" onclick="rodamientos()" id="70" value="Malo" @if(old('rodamiento') == 'Malo') checked @endif>
                            <label class="form-check-label" for="70">Malo</label>
                        </div>
                    </div>
                    <div class="col-2 mb-3">
                        <span class="input-group-text" id="basic-addon1">Mal estado ¿Por qué?</span>
                    </div>
                    <div class="col-2 mb-3">
                        <input type="text" name="mal_estado" value="{{ old('mal_estado') }}" class="form-control" placeholder="Especifique" aria-label="Username" id="71">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-2"></div>
                    <div class="col-8">
                        @error('rodamiento')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-2">
                        @error('mal_estado')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <!-- 21 fila -->
                <div class="row">
                    <div class="col-12 mb-3">
                        <p class="bg-secondary bg-gradient p-1 text-center fw-bold fs-4" id="basic-addon1">Croquis</p>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="file" class="form-control" value="{{ old('croquis')}}" name="croquis" id="72">
                    <div class="row mb-3">
                        <div class="col-3">
                            @error('croquis')
                                    <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <p class="bg-secondary bg-gradient p-1 text-center fw-bold fs-4" id="basic-addon1">Coordenadas</p>
                <div class="row mb-3">
                <textarea class="form-control" name="coordenadas" id="exampleFormControlrea1" placeholder="Coloque las coordenadas (x,y)" rows="1">{{ old('coordenadas') }}</textarea>
                @error('coordenadas')
                    <span class="text-danger">{{ $message }}</span>
                @enderror
                </div>
                <!-- 22 fila -->
                <div class="row">
                    <div class="col-12 mb-3">
                        <p class="bg-secondary bg-gradient p-1 text-center fw-bold fs-4" id="basic-addon1">Redacción de hechos</p>
                </div>
                <!-- 23 fila -->
                <div class="row">
                    <div class="mb-3">
                        <textarea class="form-control" name="redaccion_hechos" id="exampleFormControlTextarea1"placeholder="Explique con detalle lo ocurrido en el lugar del incidente" rows="8">{{ old('redaccion_hechos') }}</textarea>
                        @error('redaccion_hechos')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                      </div>
                </div>
                <!-- 24 fila -->
                <div class="row">
                    <div class="col-12 mb-3">
                        <p class="bg-secondary bg-gradient p-1 text-center fw-bold fs-4" id="basic-addon1">Fotos</p>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <input type="file" class="form-control" value="{{ old('fotos')}}" name="fotos" multiple id="73">
                    
                </div>
                <div class="row mb-3">
                    <div class="col-3">
                    @error('fotos')
                            <span class="text-danger">{{ $message }}</span>
                    @enderror
                    </div>
                </div>
                <div class="d-grid gap-2 col-2 mb-3 mx-auto">
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
            </form>
        </div>
        <script type="text/javascript">
            $('#date').datepicker({
                language: "es",
                orientation: "bottom auto",
                autoclose: true,
                todayHighlight: true,
                beforeShowYear: function(date){
                    if (date.getFullYear() == 2007) {
                        return false;
                    }
                    }
            });
            $('#6').datepicker({
                language: "es",
                orientation: "bottom auto",
                autoclose: true,
                todayHighlight: true,
                beforeShowYear: function(date){
                    if (date.getFullYear() == 2007) {
                        return false;
                    }
                    }
            });
            
            function hola(){
                    var memo=document.getElementsByName('ciclovia');
                    for(i=0; i<memo.length; i++){
                    if(memo[i].checked){
                    //var memory=memo[i].checked;
                    var memory=memo[i].value;
                    
                    if(memory == "si"){
                        $( "#59" ).prop( "disabled", false );
                        $( "#60" ).prop( "disabled", false );
                        $( "#61" ).prop( "disabled", false );
                        $( "#62" ).prop( "disabled", false );
                        $( "#63" ).prop( "disabled", false );
                        $( "#64" ).prop( "disabled", false );
                        $( "#65" ).prop( "disabled", false );
                        $( "#66" ).prop( "disabled", false );
                        $( "#67" ).prop( "disabled", false );
                        $( "#68" ).prop( "disabled", false );
                        $( "#69" ).prop( "disabled", false );
                        $( "#70" ).prop( "disabled", false );
                        $( "#71" ).prop( "disabled", false );
                        $( "#200" ).prop( "disabled", false );
                    }else{
                        $( "#59" ).prop( "disabled", true );
                        $( "#60" ).prop( "disabled", true );
                        $( "#61" ).prop( "disabled", true );
                        $( "#62" ).prop( "disabled", true );
                        $( "#63" ).prop( "disabled", true );
                        $( "#64" ).prop( "disabled", true );
                        $( "#65" ).prop( "disabled", true );
                        $( "#66" ).prop( "disabled", true );
                        $( "#67" ).prop( "disabled", true );
                        $( "#68" ).prop( "disabled", true );
                        $( "#69" ).prop( "disabled", true );
                        $( "#70" ).prop( "disabled", true );
                        $( "#71" ).prop( "disabled", true );
                        $( "#200" ).prop( "disabled", true );
                        //a los campos de texto ponles esta propiedad
                        $("#200").val('');
                        $("#71").val('');
                        $("#67").val('');
                        //a los checkbox ponles esta propiedad
                        $("#59").prop("checked", false);
                        $("#60").prop("checked", false);
                        $("#61").prop("checked", false);
                        $("#62").prop("checked", false);
                        $("#63").prop("checked", false);
                        $("#64").prop("checked", false);
                        $("#65").prop("checked", false);
                        $("#66").prop("checked", false);
                        $("#67").prop("checked", false);
                        $("#68").prop("checked", false);
                        $("#69").prop("checked", false);
                        $("#70").prop("checked", false);
                        
                    }

                    }
                }
            }
            function adios(){
                var memo=document.getElementsByName('tipo_vehiculo');
                for(i=0; i<memo.length; i++){
                    if(memo[i].checked){
                        var memory=memo[i].value;
                        if(memory === "Transporte Publico"){
                            $( "#997" ).prop( "disabled", false );
                            $( "#998" ).prop( "disabled", true );
                            $( "#999" ).prop( "disabled", true );
                            $( "#1999" ).prop( "disabled", true );
                            $("#998").val('');
                            $("#999").val('');
                            $("#1999").val('');
                            
                        }else if(memory== "Transporte De Carga"){
                            $( "#998" ).prop( "disabled", false );
                            $( "#997" ).prop( "disabled", true );
                            $( "#999" ).prop( "disabled", true );
                            $( "#1999" ).prop( "disabled", true );
                            $("#997").val('');
                            $("#999").val('');
                            $("#1999").val('');
                            
                        }else if(memory== "Transporte Particular"){
                            $( "#999" ).prop( "disabled", false );
                            $( "#997" ).prop( "disabled", true );
                            $( "#998" ).prop( "disabled", true );
                            $( "#1999" ).prop( "disabled", true );
                            $("#997").val('');
                            $("#998").val('');
                            $("#1999").val('');
                            
                        }else if(memory== "otro")
                        {
                            $( "#1999" ).prop( "disabled", false );
                            $( "#997" ).prop( "disabled", true );
                            $( "#998" ).prop( "disabled", true );
                            $( "#999" ).prop( "disabled", true );
                            $("#997").val('');
                            $("#998").val('');
                            $("#999").val('');
                        }
                    }
                    
                }
            }

            function rodamientos(){
                var rodamiento = document.getElementsByName('rodamiento');
                for(i=0; i<rodamiento.length; i++){
                    if(rodamiento[i].checked){
                        var value = rodamiento[i].value;
                        if(value != "Bueno"){
                            $( "#71" ).prop( "disabled", false );
                        }else{
                            $( "#71" ).prop( "disabled", true );
                            $("#71").val('');
                        }
                    }
                }
            }

        </script>
    </body>
    </html>